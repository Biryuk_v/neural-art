#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.


import logging
import os
import os.path
import random
import urllib
import telegram
from telegram.error import NetworkError, Unauthorized
from time import sleep
from subprocess import *

neural_path='neural_artistic_style.py'
sample_path='images/'
style_path='images'
out_path=''


helplist = dict()
helplist["/help"] = "Печатаю хелп"
helplist["/start"] = "здоровуюсь и начинаю работу (на самом деле нет)"
helplist["/paint"] = "моя основная задача - вы выбираете стиль: Ван Гог, Ренуар или Моне, скидываете пикчу, и я рису ее как тру-художник"

command = dict()
command["/help"] = "print_help(bot, chat_id)"
command["/start"] = "print \" Yo!\" "
command["/paint"] = "bot.sendMessage(chat_id=chat_id, text=\"Окей, выбери стиль между /Vinsent /Claude или /Renuar \") ;" \
                    "Paintr_switch(bot,chat_id, update_id)"

stylelist = dict()
stylelist["/Vinsent"] = "start_painting(\"/Vinsent\", bot, chat_id, update_id)"
stylelist["/Claude"] = "start_painting(\"/Claude\", bot, chat_id, update_id)"
stylelist["/Renuar"] = "start_painting(\"/Renuar\", bot, chat_id, update_id)"


def print_help(bot, chat_id):
    for elem in helplist.keys():
        bot.sendMessage(chat_id=chat_id, text=elem + " - " + helplist[elem])
    return

def Paintr_switch(bot,chat_id, update_id):
    for update in bot.getUpdates(offset=update_id, timeout=100):
        chat_id = update.message.chat_id
        update_id = update.update_id + 1
        message = update.message.text
        if (message in command):
            exec (command[message])

        if not (message in stylelist):
            bot.sendMessage(chat_id=chat_id, text="Я такого стиля не знаю( давай еще раз: \Vinsent \Cloudt или \Renuar")
            Paintr_switch(bot, chat_id, update_id)
            #bot.sendChatAction(chat_id=chat_id, action=telegram.ChatAction.TYPING)
        else:
            bot.sendMessage(chat_id=chat_id, text="Окей, давай сюда картинку (файлом давай)")
            exec (stylelist[message])
    return

def start_painting(style, bot, chat_id, update_id):
    for update in bot.getUpdates(offset=update_id, timeout=100):
        chat_id = update.message.chat_id
        update_id = update.update_id + 1
        message = update.message.text
        if (message in command):
            exec (command[message])
            return
        file = bot.getFile(update.message.document.file_id)
        os.makedirs(sample_path + file.file_id)
        urllib.urlretrieve(file.file_path, sample_path + file.file_id + "/" + "pic.jpg")
        bot.sendMessage(chat_id=chat_id, text="ну, я погнал работать, подожди пару минут")
        bot.sendChatAction(chat_id=chat_id, action=telegram.ChatAction.TYPING)
        #push photo on conveer for neural algo
        operation = 'python ' + neural_path + ' --subject ' + sample_path + file.file_id + "/" + "pic.jpg" + ' --style ' + style_path + style + ".jpg" + ' --iterations ' + "100" + ' --output ' + sample_path + file.file_id + "/out.png"
        os.system(operation)
        #proc = Popen(
        #    operation,
        #    shell=True,
        #    #stdout=PIPE, stderr=PIPE, stdin=PIPE
        #)
        bot.sendMessage(chat_id=chat_id, text="Готово, радуйся")
        bot.sendPhoto(chat_id=chat_id, photo=open(sample_path + file.file_id +  "/out.png", 'rb'))
        #bot.sendMessage(chat_id=chat_id, text="Готово, радуйся")

    return

def main():
    # Telegram Bot Authorization Token
    bot = telegram.Bot('224260929:AAHLhVati7_R6D3ZWEBjRvG-6H_owjLgzpU')
    try:
        update_id = bot.getUpdates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            update_id = echo(bot, update_id)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            update_id += 1

stupid_ans = dict()
#stupid_ans[8] = "Шкура, перестань"
stupid_ans[1] = "Попробуй /help"
#stupid_ans[9] = "Что за дичь? давай нормально"
#stupid_ans[5] = "Вилкой в глаз или в жопу раз?"
#stupid_ans[7] = "Кстати, Aндрей питух, а теперь о главном"
stupid_ans[2] = "Я тебя не понимаю, попробуй /help"
stupid_ans[3] = telegram.Emoji.PILE_OF_POO + "Чет не очень вышло( Попробуй еще или вызови /help"


def echo(bot, update_id):

    for update in bot.getUpdates(offset=update_id, timeout=10):
        # chat_id is required to reply to any message
        chat_id = update.message.chat_id
        update_id = update.update_id + 1
        message = update.message.text
        if message and not (message in command) :
            bot.sendMessage(chat_id=chat_id, text=stupid_ans[random.randrange(1,4)])
            #bot.sendChatAction(chat_id=chat_id, action=telegram.ChatAction.TYPING)
        elif message:
            exec(command[message])
            return update_id
		#bot.sendMessege(chat_id=chat_id, )

    return update_id


if __name__ == '__main__':
    main()
