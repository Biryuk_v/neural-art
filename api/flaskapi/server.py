from flask import Flask, jsonify
from flask import make_response
from flask import request
import subprocess
import urllib
import os
import os.path

neural_path='~/neural-artistic-style/neural_artistic_style.py'
sample_path='~/n-a-s/sourse_image/sample/'
style_path='~/n-a-s/sourse_image/style/'
out_path='~/n-a-s/output_image/'
log_path='~/n-a-s/hystory/'

app = Flask(__name__)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(502)
def not_found(error):
    return make_response(jsonify({'error': 'Bad Getway'}), 502)


app = Flask(__name__)

tasks = [
#    {
#        'task_id': 1,
#        'sourse_img' : 'https://pp.vk.me/c628527/v628527564/bd3/eBXLto6W2fQ.jpg'
#        'style_img': 'http://lh6.ggpht.com/HlgucZ0ylJAfZgusynnUwxNIgIp5htNhShF559x3dRXiuy_UdP3UQVLYW6c=s1200' 
#    }
]

@app.route('/image/<int:task_id>', methods=['GET'])
def get_task(task_id):
    if not os.path.exists(out_path + task_id + '.png'):
        abort(404)
    return os.path.exists(out_path + task_id + '.png')

@app.route('/image/<int:task_id>', methods=['DELETE'])
def del_task(task_id):
    if not os.path.exists(out_path + task_id + '.png'):
        abort(502)
    if not (os.path.exists(sample_path + task_id + '.jpg') and os.path.exists(style_path + task_id + '.jpg')):
        abort(404)
    os.remove(out_path + task_id + '.png')
    os.remove(sample_path + task_id + '.jpg')
    os.remove(style_path + task_id + '.jpg')    
    return jsonify({'result': True})


@app.route('/image/<int:iteration>', methods=['POST'])
def create_task():
    if not request.json:
        abort(404)
    last_save = open(log_path + 'last.txt', 'r')
    last_id = last_save.readline()
    last_save.close()
    task_id = last_id + 1
    last_save = open(log_path + 'last.txt', 'w')
    last_save.write(task_id)
    task = {
        'id': task_id,
        'sourse_img': request.json['sourse_img'],
        'style_img': request.json.get('style_img'),
    }
    log_file = open(log_path + 'log.txt', 'a')
    log_file.write(jsonify({task}) + '\n')
    log_file.close()
    sourse = urllib.urlopen(task.sourse_img).read()
    style = urllib.urlopen(task.style_img).read()
    f = open(sample_path + task_id + '.jpg', "wb")
    f.write(sourse)
    f.close()
    f = open(style_path + task_id + '.jpg', "wb")
    f.write(sourse)
    f.close()
    operation = 'python' + neural_path + ' --subject ' + sample_path+task_id + ' --style ' + style_path + task_id + ' -- iterations ' + iteration + ' --output ' + out_path + task_id
    proc = Popen(
        operation,
        shell=True,
        stdout=PIPE, stderr=PIPE, stdin=PIPE
    )
    tasks.append(task)
    return jsonify({'Post secsessful'}), 201            
    

if __name__ == '__main__':
    app.run(debug=True)

